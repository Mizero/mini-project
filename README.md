# Mini-Project


1
C++ individual Mini Project
1.1
Ircad IHU R&D team activities
In our team we develop applications that focus mainly on laparoscopic image
guided surgery. For that it is of capital importance that we understand a variety
of subjects related to computer vision and other main AI subjects. In this
project we will focus on two important topics :
1. Optical tracking, which is used to follow the position of important struc-
tures in the image through the surgical procedure. For now, we will limit
this subject to optical tracking using markers
2. Image segmentation, we have access to a numerous variety of medical
scanning devices such as c-Arms (and more precisely Zeego) or computed
topographies (CT). Such scanning devices generate 3D images which need
to be processed before we are able to extract important structures and use
them on Augmented reality, simulation and other software. This process
is called 3D image segmentation.
1.2
Overall objective
Implement a pipeline to perform 3D visualization with optical tracking in 3
steps :
1. Optical tracking with a printed marker.
2. Bone structure segmentation on a 3D CT-image.
3. Visualize the bone structre and update its position with the tracking in-
formation.
Each step is limited to a time of work of one week.
We will provide you with every needed data and with VRRender which is a
homemade application which allows you to visualize a vtk 3D image or mesh.
1.3
Technical details
For each Milestone, we expect you to divide it in as many issues as you see fit,
and create a git branch for each of the issues you created progressively.
Each one of you will work independently and a summary meeting will be
held at the end of each milestone, in order to discuss your different approaches,
difficulties and point of view.
1.3.1
C++
Language1.4
Data needed
• Provided CT Scan (VTK file format).
• ARUCO tag sheet.
1.5
Material needed
• A Webcam.
• Printed tag sheet.
• A printed chessboard (you’ll need a rigid flat surface)
1.6
Software needed
- A VRRender installation (homemade software).2
Milestone description
2.1
2.1.1
Milestone 1 : Tracking of ARuco tags
Description
Use OpenCV to create an application to track an Aruco tag, the result of this
Milestone is the current 3D transformation matrix of the tag. Previously printed
tags will be used here.
1. Use OpenCV to calibrate your webcam with the printed chessboard (it
needs to be fixed on a rigid surface), you can use this documentation on
camera calibration.
2. Print an aruco marker and track, with a live video, the marker in 2D
3. Convert this to a 3D tracker, using OpenCV’s solvePnP function, to give
the marker’s 3D position in camera coordinates.
4. Visualize that 3D tracking is correct. This can be done by overlaying the
live camera images with 3 lines (the axes of the aruco tag). See attached
image for an example.¡br/¿ This last step important because it will get
you in the habit of plotting intermediate stages. This is very important
to catch bugs early.
2.1.2
Technical details
A detailed Aruco tag tracking using OpenCV tutorial can be foundhere
2.1.3
Duration
1 week
2.2
2.2.1
Milestone 2 : 3D Image segmentation
Description
Use ITK to segment an image and extract the bone structure and then mesh it.
Hint : You can make use of the provided VRRender software to visualize
your input and asses the quality of your results
2.2.2
Technical description
To extract the bone structure you can use a binary 3D image filter such as this
one. you’ll need to adjust the threshold progressively to find the right value for
it. In order to create a mesh from a 3D binary image (which will be the output
of your ThresholdImageFilter) you can take a look at this example2.2.3
Duration
1 week
2.3
2.3.1
Milestone 3 : Visualization and 3D image movement.
Description
Use vtk to visualize the output vtk file (output of Milestone 2) and modify its
position according to the transformation matrix (using the Milestone 1 code).
To load a .vtk mesh file format, you can take inspiration from the many
available VTK examples, most of them use vtkPolyDataReader. Then change
mesh’s position and rotation merge according to the current 3D transformation
matrix using the code from Milestone 1.
2.3.2
1 week
Duration
